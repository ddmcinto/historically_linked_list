#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//not thread safe
static inline unsigned int rand32() {
    //actually only 30 bits, but good enough
    return (rand() & 0x7FFF) | ((rand() & 0x7FFF) << 15);
}

static inline unsigned long long rand64() {
    //actually only 60 bits, but good enough
    return ((rand() & 0x5FFFULL) << 0 ) | ((rand() & 0x5FFFULL) << 15) |
           ((rand() & 0x5FFFULL) << 30) | ((rand() & 0x5FFFULL) << 45);
}

typedef struct node {
    unsigned long long value;
    struct node *next;
    struct node *jump;
} Node;

static inline void insertAfter(Node *prev, unsigned long long value, Node *sublistEnd) {
    Node *new = malloc(sizeof(Node));
    new->value = value;
    new->jump = prev->next;
    new->next = prev->next;

    if (prev->jump == sublistEnd) {
        prev->jump = new;
    }
    prev->next = new;
}

static void insert(Node *head, unsigned long long value, unsigned long long *steps) {
    Node *cur = head;
    Node *sublistEnd = NULL;
    while (cur->next != NULL) {
        ++(*steps);
        //don't need to check cur->jump != NULL since cur->next != NULL
        if ((cur->jump)->value <= value) {
            cur = cur->jump;
        }
        else if ((cur->next)->value <= value) {
            sublistEnd = cur->jump;
            cur = cur->next;
        }
        else {
            break;
        }
    }
    insertAfter(cur, value, sublistEnd);
}

static Node *search(Node *head, unsigned long long value, unsigned long long *steps) {
    Node *cur = head;
    while (cur->next != NULL) {
        ++(*steps);
        //don't need to check cur->jump != NULL since cur->next != NULL
        if ((cur->jump)->value <= value) {
            cur = cur->jump;
        }
        else if ((cur->next)->value <= value) {
            cur = cur->next;
        }
        else {
            break;
        }
    }
    return cur;
}

static unsigned long long build(unsigned long long size, Node **list) {
    unsigned long long steps = 0;
    Node *head = malloc(sizeof(Node));
    head->next = 0;
    head->jump = 0;
    head->value = 0;

    for (unsigned long long i = 0; i < size; ++i) {
        unsigned long long value = rand64();

        insert(head, value, &steps);
    }

    *list = head->next;
    free(head);
    return steps;
}

int main() {
    srand (time(NULL));

    unsigned long long size;
    printf("size: ");
    scanf("%llu", &size);

    int iterations;
    printf("iterations: ");
    scanf("%d", &iterations);

    int builds;
    printf("builds: ");
    scanf("%d", &builds);

    printf("\n");
    Node *head;
    unsigned long long total_reLinked = 0ULL;
    for (int b = 0; b < builds; ++b)
    {
        if (!(b & 0xF))
        {
            printf("building (%d) ...\r", b);
        }
        build(size, &head);

        printf("searching ...");
        for (int i = 0; i < iterations; ++i)
        {
            if (!(i & 0x3FFFF))
            {
                putchar('.');
            }
            unsigned long long value = rand64();
            search(head, value, &total_reLinked);
        }

        printf("\r");
    }
    printf("total_reLinked = %llu\navg_reLinked = %llu\n", total_reLinked, total_reLinked/(iterations * builds));
    return 0;
}
