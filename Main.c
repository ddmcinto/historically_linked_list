#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define LINKED 0

#define FORK_POINT (1 << 12)
#define NTHREADS 8
#define PRNG_BUFSZ 32

//not thread safe
static inline unsigned int rand32() {
	//actually only 30 bits, but good enough
	return (rand() & 0x7FFF) | ((rand() & 0x7FFF) << 15);
}

//thread-safe random
static inline unsigned int rand32_r(struct random_data *buf) {
	int32_t result1, result2;
	random_r(buf, &result1);
	random_r(buf, &result2);
	//actually only 30 bits, but good enough
	return (result1 & 0x7FFF) | ((result2 & 0x7FFF) << 15);
}

typedef struct relink {
	int value;
	struct relink *next;
	struct relink *jump;
} ReLink;

void insert_reLinked(ReLink *reLink, int value) {
	ReLink *new = malloc(sizeof(ReLink));
	new->next = reLink->next;
	new->jump = reLink->next;
	new->value = value;
	reLink->next = new;

	if (!reLink->jump) {
		reLink->jump = new;
	}
}

static inline unsigned long long int search_reLinked(ReLink **head, int value) {
	unsigned long long int steps = 1;
	while ((*head)->next && ((*head)->next)->value > value) {
		if ((*head)->jump && ((*head)->jump)->value >= value) {
			(*head) = (*head)->jump;
		}
		else {
			(*head) = (*head)->next;
		}
		++steps;
	}
	return steps;
}

struct thread_data {
	const unsigned long long size;
	ReLink *const head;
	pthread_rwlock_t *const rwlock;
};

void *threaded_build(void *arg) {
	struct thread_data *data = (struct thread_data *)arg;

	struct random_data *rand_state = (struct random_data*)calloc(1, sizeof(struct random_data));
	char *rand_statebuf = (char*)calloc(1, PRNG_BUFSZ);
	initstate_r((time(NULL) ^ pthread_self()), rand_statebuf, PRNG_BUFSZ, rand_state);

	for (unsigned long long i = 0; i < data->size; ++i) {
		if (!(i % 1000000)) {
			//printf("thread %lu at %llu\n", pthread_self(), i);
		}
		unsigned int value = rand32_r(rand_state);

		ReLink *cur = data->head;
		pthread_rwlock_rdlock(data->rwlock);
		search_reLinked(&cur, value);
		pthread_rwlock_unlock(data->rwlock);

		pthread_rwlock_wrlock(data->rwlock);
		search_reLinked(&cur, value);
		insert_reLinked(cur, value);
		pthread_rwlock_unlock(data->rwlock);
	}

	free(rand_state);
	free(rand_statebuf);
}

void build(unsigned long long size, ReLink **list) {

	ReLink *head = malloc(sizeof(ReLink));
	head->next = 0;
	head->jump = 0;
	head->value = 0;

	for (int i = 0; i < ((FORK_POINT > size) ? FORK_POINT : size); ++i) {
		unsigned int value = rand32();

		ReLink *cur = head;
		search_reLinked(&cur, value);
		insert_reLinked(cur, value);
	}
	if (FORK_POINT > size) {
		*list = head->next;
		free(head);
		return;
	}
	//start multi-threading once we are large enough that colisions shouldn't be too frequent
	pthread_t* thread_ids = calloc(NTHREADS, sizeof(pthread_t));
	pthread_rwlock_t *rwlock = malloc(sizeof(pthread_rwlock_t));
	pthread_rwlock_init(rwlock, NULL);

	struct thread_data data = {
		.size = (size - FORK_POINT)/NTHREADS,
		.head = head,
		.rwlock = rwlock
	};
	for (int t = 0; t < NTHREADS; t++) {
		pthread_create(&thread_ids[t], NULL, &threaded_build, &data);
	}
	for (int t = 0; t < NTHREADS; t++) {
		pthread_join(thread_ids[t], NULL);
	}
	pthread_rwlock_destroy(rwlock);

	free(thread_ids);

	*list = head->next;
	free(head);
}

int main() {
	srand (time(NULL));

	unsigned long long size;
	printf("size: ");
	scanf("%llu", &size);

	int iterations;
	printf("iterations: ");
	scanf("%d", &iterations);

	ReLink *head;
	build(size, &head);

	unsigned long long int total_reLinked = 0ULL;
	for (int i = 0; i < iterations; ++i) {
		unsigned int value = rand32();
		ReLink *cur = head;
		total_reLinked += search_reLinked(&cur, value);
	}
	printf("total_reLinked = %llu\navg_reLinked = %llu\n", total_reLinked, total_reLinked/iterations);
	return 0;
}
