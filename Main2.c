#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LINKED 0
#define DEBUG 0

/* DESCRIPTION:
 *
 * A test of the performance of a sorted linked list with a small modification
 *
 * personal analysis of algorith for searching would indicate average performance of _at least_  O( log(n) * log(n) )
 *
 * Randomly generates a list of a given size n,											O( n )
 * sorts it,																			O( n * log(n) )
 * artificially re-creates the 'jumps' based on the order they were inserted,			O( n )
 * searches for a random element, recording the number of steps we took					O( log(n) * log(n) )
 * repeats the search, with another random element m times to get an average			O( m * log(n) * log(n) )
 *
 * Main.c is a more accurate test, but struggles to generate a large list since
 * it is esentially running insertion sort with log(n)^2 time insertion,
 * which is not great when n gets rather large. Instead we use the strategy above,
 * which in theory should produce the same resulting list for us to search through.
 */

static inline unsigned int rand32() {
	//actually only 30 bits, but good enough
	return (rand() & 0x7FFF) | ((rand() & 0x7FFF) << 15);
}

typedef struct relink {
	unsigned int value;
	struct relink *next;

	//done to save memory, since we are capped at 70 000 000 elements by memory constraints
	union {

		//points to what was the next element when this was inserted
		struct relink *jump;

		//used to artificially re-construct the jump pointers after we've generated a sorted list
		unsigned int insert_order;
	};
} ReLink;

static inline ReLink *append(ReLink *reLink, unsigned int value, unsigned int insert_order) {
	ReLink *new = malloc(sizeof(ReLink));
	new->next = 0;
	new->value = value;
	new->insert_order = insert_order;

	reLink->next = new;

	return new;
}

ReLink *quickSort(ReLink **list) {
	ReLink *pivot = *list;

	ReLink head = { .next = NULL, .insert_order = 0, .value = 0};

	ReLink *gt_end = &head, *lt_end = pivot;
	for (ReLink *cur = pivot->next; cur; cur = cur->next) {
		if (cur->value > pivot->value) {
			gt_end->next = cur;
			gt_end = cur;
		}
		else {
			lt_end->next = cur;
			lt_end = cur;
		}
	}
	gt_end->next = 0;
	lt_end->next = 0;

	if (head.next) {
		gt_end = quickSort(&(head.next));
	}
	if (pivot->next) {
		lt_end = quickSort(&(pivot->next));
	}

	gt_end->next = pivot;
	*list = head.next;

	return lt_end;
}

//use the insert_order values to re-create what the jump pointers
// would have looked like if the list was generated step by step
void assign_jumps(ReLink *reLink) {
	// used as a stack
	struct link {
		ReLink *val;
		struct link *next;
	} *jump_list = malloc(sizeof(struct link));
	jump_list->val = reLink;
	jump_list->next = NULL;

	// keep pushing nodes in reLink onto a stack until we find a node that is older than the previous nodes
	// that node was the node immediately after the younger nodes on the stack at their time of insertion
	// therefore, their jump pointers point to it.

	// Note that nodes on the stack are always inserted oldest to youngest. That is, when a node
	// is popped it is always the youngest node currently on the stack
	for (ReLink *cur = reLink; cur; cur = cur->next) {
		// while jump_list->val is younger than cur, pop from jump list, and set jump_list->val's jump to cur
		while (jump_list && (jump_list->val)->insert_order > cur->insert_order) {
			(jump_list->val)->jump = cur;
			struct link *old = jump_list;
			jump_list = jump_list->next;
			free(old);
		}

		// push cur onto our stack
		struct link *new = malloc(sizeof(struct link));
		new->next = jump_list;
		new->val = cur;
		jump_list = new;
	}

	// empty the rest of the stack. These nodes are the nodes which had no next at their
	// time of insertion. That is, they were inserted at the end of the list.
	while(jump_list->next) {
		struct link *old = jump_list;
		jump_list = jump_list->next;
		(jump_list->val)->jump = old->val;
		free(old);
	}

	free(jump_list);
}

void build(unsigned int size, ReLink **list) {

	printf("generating values ");
	ReLink head = { .next = NULL, .insert_order = 0, .value = 0};
	ReLink *cur = &head;
	for (unsigned int i = 0; i < size; ++i) {
		if (!(i & 0x3FFFFF))
		{
			putchar('.');
		}
		unsigned int value = rand32();
		cur = append(cur, value, i);
	}
	//erase the line
	//windows command prompt doesn't work with terminal escape codes
	printf("\r                                       \r");

	printf("sorting ...");
	quickSort(&(head.next));
	printf("\r                                       \r");

	printf("linking ...");
	assign_jumps(head.next);
	printf("\r                                       \r");

	*list = head.next;
}

static inline unsigned int search_reLinked(ReLink **head, unsigned int value) {
	unsigned int steps = 1;
	while ((*head)->next && ((*head)->next)->value > value) {
		if ((*head)->jump && ((*head)->jump)->value > value) {
			(*head) = (*head)->jump;
		}
		else {
			(*head) = (*head)->next;
		}
		++steps;
	}
	return steps;
}

int main() {
	srand (time(NULL));

	unsigned int size;
	printf("size: ");
	scanf("%u", &size);

	int iterations;
	printf("iterations: ");
	scanf("%d", &iterations);

	int builds;
	printf("builds: ");
	scanf("%d", &builds);

	ReLink *head;
	unsigned long long total_reLinked = 0ULL;
	for (int b = 0; b < builds; ++b)
	{
		if (!(b & 0xF))
		{
			printf("building ... (%d)\r", b);
		}
		build(size, &head);

		printf("searching ");

		for (int i = 0; i < iterations; ++i) {
			if (!(i & 0x3FFFF))
			{
				putchar('.');
			}

			unsigned int value = rand32();
			ReLink *cur = head;
			total_reLinked += search_reLinked(&cur, value);
		}
	}

	printf("\ntotal_reLinked = %llu\navg_reLinked = %llu\n", total_reLinked, total_reLinked/(iterations * builds));
	return 0;
}
