A test of the performance of a sorted linked list with a small modification

personal analysis of algorith for searching would indicate average performance of _at worst_  O( log(n) * log(n) )

Main.c is an exact test with multi-threading but slow, while Main2.c is fast, but artificially makes the necessary changes **after** generating the list
Main3.c uses a slightly different algorithm which generates a slightly different modified linked list, which I believe should be faster than Main.c and Main2.c for searching
